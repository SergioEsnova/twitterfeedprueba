using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace TwitterFeed
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello!!");

            string twitterHandle = "";
            if (args.Length == 1)
            {
                twitterHandle = args[0];
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("No has pasado un nombre de usuario como argumento.");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Uso: TwitterFeed [twitterHandle]");
                Console.WriteLine("Por ejemplo: TwitterFeed.exe VisualStudio");
                Console.ResetColor();
               // Environment.Exit(0);
                Console.ReadLine();
                Environment.Exit(0);
            }

            string OAuthConsumerKey = "Sqwn3YFdFHTq4Faec04n6z51P";
            string OAuthConsumerSecret = "HXqVhUWp6z3gFKbSNfRbdwna9L4UJ3yje9kMujkyDB6AYmFlJn";

            
           

            Twitter twitter = new Twitter(OAuthConsumerKey, OAuthConsumerSecret);
            var tweetsTask = twitter.GetTweets(twitterHandle, 10);
            tweetsTask.Wait();
            var twitts = tweetsTask.Result;

            foreach (var t in twitts)
            {
                //https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline.html
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(t["created_at"].ToString());
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(" -> ");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write(t["text"].ToString());
                Console.Write("\n");
                Console.ResetColor();
            }

            Console.ReadLine();
        }

        static async Task<string> GetAccessToken()
        {
            string OAuthConsumerKey = "Sqwn3YFdFHTq4Faec04n6z51P";
            string OAuthConsumerSecret = "HXqVhUWp6z3gFKbSNfRbdwna9L4UJ3yje9kMujkyDB6AYmFlJn";
            var httpClient = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, "https://api.twitter.com/oauth2/token");
            var customerInfo = Convert.ToBase64String(new UTF8Encoding()
                                      .GetBytes(OAuthConsumerKey + ":" + OAuthConsumerSecret));
            request.Headers.Add("Authorization", "Basic " + customerInfo);
            request.Content = new StringContent("grant_type=client_credentials",
                                                    Encoding.UTF8, "application/x-www-form-urlencoded");

            HttpResponseMessage response = await httpClient.SendAsync(request);

            string json = await response.Content.ReadAsStringAsync();
            var serializer = new JavaScriptSerializer();
            dynamic item = serializer.Deserialize<object>(json);
            return item["access_token"];
        }

        static async Task<IEnumerable<string>> GetTweets(string userName, int count, string accessToken = null)
        {
            if (accessToken == null)
            {
                accessToken = await GetAccessToken();
            }

            var requestUserTimeline = new HttpRequestMessage(HttpMethod.Get,
                string.Format("https://api.twitter.com/1.1/statuses/user_timeline.json?count={0}&screen_name={1}&trim_user=1&exclude_replies=1", count, userName));

            requestUserTimeline.Headers.Add("Authorization", "Bearer " + accessToken);
            var httpClient = new HttpClient();
            HttpResponseMessage responseUserTimeLine = await httpClient.SendAsync(requestUserTimeline);
            var serializer = new JavaScriptSerializer();
            dynamic json = serializer.Deserialize<object>(await responseUserTimeLine.Content.ReadAsStringAsync());
            var enumerableTweets = (json as IEnumerable<dynamic>);

            if (enumerableTweets == null)
            {
                return null;
            }
            return enumerableTweets.Select(t => (string)(t["text"].ToString()));
        }
    }
}
